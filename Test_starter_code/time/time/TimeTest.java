/**
 * Connor Muray
 * 991553779
 * 
 */
//
//s
package time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {

	@Test 
	public void testGetPassword() {
		String password = Time.getPassword("SheridanCollege12");
		assertTrue("The passowrd is not long enough", password.length()>=8 && Time.passHasNum(password) == true);	
	}
}
	/*
	 * @Test public void testGetTotalSecondsRegular() { int totalSeconds =
	 * Time.getTotalSeconds("01:01:01");
	 * assertTrue("the time provided does not match the result", totalSeconds ==
	 * 3661); }
	 * 
	 * @Test public void testGetTotalMilliseconds() { int totalMilliseconds =
	 * Time.getMilliseconds("12:05:50:05");
	 * assertTrue("The time provied does not match the result", totalMilliseconds ==
	 * 5); }
	 * 
	 * @Test(expected=NumberFormatException.class) public void
	 * testGetTotalMillisecondsException() { int totalMilliseconds =
	 * Time.getMilliseconds("12:05:50:0a"); fail("invaild number"); }
	 * 
	 * @Test public void testGetTotalMillisecondsBoundryIn() { int totalMilliseconds
	 * = Time.getMilliseconds("12:05:50:77");
	 * assertTrue("The time is correctly macthing", totalMilliseconds == 77); }
	 * 
	 * @Test public void testGetTotalMillisecondsBoundryOut() { int
	 * totalMilliseconds = Time.getMilliseconds("12:05:50:10");
	 * assertTrue("The time is correctly macthing", totalMilliseconds == 00); }
	 */
